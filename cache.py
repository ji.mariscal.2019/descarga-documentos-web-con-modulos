from robot import Robot

class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            self.cache[url] = Robot(url)
            self.cache[url].retrieve()

    def show(self, url):
        self.retrieve(url)
        print(self.cache[url].content())

    def show_all(self):
        print("URLs descargadas:")
        for url in self.cache:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

if __name__ == '__main__':
    print("Prueba clase Cache")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('http://gsyc.urjc.es/')
    c.show_all()