import urllib.request

class Robot:
    def __init__(self, url):
        self.url = url
        print(self.url)
        self.downloaded = False
        self.content_data = None

    def retrieve(self):
        if not self.downloaded:
            with urllib.request.urlopen(self.url) as response:
                self.content_data = response.read().decode('utf-8')
            print("Descargando", self.url)
            self.downloaded = True
        else:
            print("El contenido ya ha sido descargado anteriormente.")

    def show(self):
        self.retrieve()
        print(self.content_data)

    def content(self):
        self.retrieve()
        return self.content_data

if __name__ == '__main__':
    print("Test Robot class")
    robot = Robot('http://gsyc.urjc.es/')
    print(robot.url)
    robot.show()
    robot.retrieve()
    robot.retrieve()
